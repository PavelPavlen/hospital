﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DAL;
using BusinessLogic;
using System.Data.SqlClient;

namespace Hospital
{
    public partial class Form1 : Form
    {
        DbOperations dbo = new DbOperations();
        Form2 f2;
        public Form1()
        {
           // DbOperations dbo = new DbOperations();

            InitializeComponent();
        }

        private void AddTheColumnsForDoctors()
        {
            dataGridView1.ColumnCount = 4;
            dataGridView1.Visible = true;
            dataGridView1.Columns[0].Name = "ID";
            dataGridView1.Columns[1].Name = "Name";
            dataGridView1.Columns[2].Name = "Surmane";
            dataGridView1.Columns[3].Name = "Speciality";

        }

        private void AddTheColumnsForVisitors()
        {
            dataGridView1.ColumnCount = 4;
            dataGridView1.Visible = true;
            dataGridView1.Columns[0].Name = "ID";
            dataGridView1.Columns[1].Name = "Name";
            dataGridView1.Columns[2].Name = "Surmane";
            dataGridView1.Columns[3].Name = "Dicease";

        }

        //private void Form1_Load(object sender, EventArgs e)
        //{
        //    //label2.BackColor = Color.Transparent;
        //    SqlCommand selectAll = new SqlCommand("SELECT * FROM PMp32", DbOperations.conn);
        //    DbOperations.conn.Open();
        //    SqlDataReader reader = selectAll.ExecuteReader();
        //    while (reader.Read())
        //    {
        //        dataGridView1.Rows.Add(

        //            reader[1].ToString(),
        //            reader[2].ToString(),
        //            reader[3].ToString()
        //          );
        //    }
        //    DbOperations.conn.Close();
        //}

        private void ShowTheTable()
        {
            while (dbo.reader.Read())
            {
                dataGridView1.Rows.Add(
                    dbo.reader[0].ToString(),
                    dbo.reader[1].ToString(),
                    dbo.reader[2].ToString(),
                    dbo.reader[3].ToString()
                  );
            }

        }

        private void doctors_button_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            AddTheColumnsForDoctors();
            dbo.TheDoctorsDb();
            ShowTheTable();
            dbo.Close();
        }

        private void visitors_button_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            AddTheColumnsForVisitors();
            dbo.TheVisitorsDb();
            ShowTheTable();
            dbo.Close();
        }

        private void delete_doctors_button_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            AddTheColumnsForDoctors();
            dbo.DeleteDoctors();
            ShowTheTable();
            dbo.Close();
            messangebox.Text = "successfully deleted";

        }

        private void detele_visitors_button_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            AddTheColumnsForVisitors();
            dbo.DeleteVisitors();
            ShowTheTable();
            dbo.Close();
            messangebox.Text = "successfully deleted";

        
        }

        

        private void addnewvisitor_button_Click(object sender, EventArgs e)
        {
            f2 = new Form2();
            f2.ShowDialog();            

        }

    }
}
