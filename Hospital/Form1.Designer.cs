﻿namespace Hospital
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.doctors_button = new System.Windows.Forms.Button();
            this.visitors_button = new System.Windows.Forms.Button();
            this.delete_doctors_button = new System.Windows.Forms.Button();
            this.detele_visitors_button = new System.Windows.Forms.Button();
            this.messangebox = new System.Windows.Forms.Label();
            this.addnewvisitor_button = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(61, 64);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(422, 185);
            this.dataGridView1.TabIndex = 0;
            // 
            // doctors_button
            // 
            this.doctors_button.Location = new System.Drawing.Point(117, 4);
            this.doctors_button.Name = "doctors_button";
            this.doctors_button.Size = new System.Drawing.Size(104, 24);
            this.doctors_button.TabIndex = 1;
            this.doctors_button.Text = "ShowDoctors";
            this.doctors_button.UseVisualStyleBackColor = true;
            this.doctors_button.Click += new System.EventHandler(this.doctors_button_Click);
            // 
            // visitors_button
            // 
            this.visitors_button.Location = new System.Drawing.Point(273, 4);
            this.visitors_button.Name = "visitors_button";
            this.visitors_button.Size = new System.Drawing.Size(145, 23);
            this.visitors_button.TabIndex = 2;
            this.visitors_button.Text = "ShowVisitors";
            this.visitors_button.UseVisualStyleBackColor = true;
            this.visitors_button.Click += new System.EventHandler(this.visitors_button_Click);
            // 
            // delete_doctors_button
            // 
            this.delete_doctors_button.Location = new System.Drawing.Point(117, 34);
            this.delete_doctors_button.Name = "delete_doctors_button";
            this.delete_doctors_button.Size = new System.Drawing.Size(104, 24);
            this.delete_doctors_button.TabIndex = 3;
            this.delete_doctors_button.Text = "Delete Doctors";
            this.delete_doctors_button.UseVisualStyleBackColor = true;
            this.delete_doctors_button.Click += new System.EventHandler(this.delete_doctors_button_Click);
            // 
            // detele_visitors_button
            // 
            this.detele_visitors_button.Location = new System.Drawing.Point(273, 34);
            this.detele_visitors_button.Name = "detele_visitors_button";
            this.detele_visitors_button.Size = new System.Drawing.Size(145, 24);
            this.detele_visitors_button.TabIndex = 4;
            this.detele_visitors_button.Text = "Delete Visitors";
            this.detele_visitors_button.UseVisualStyleBackColor = true;
            this.detele_visitors_button.Click += new System.EventHandler(this.detele_visitors_button_Click);
            // 
            // messangebox
            // 
            this.messangebox.AutoSize = true;
            this.messangebox.Location = new System.Drawing.Point(544, 161);
            this.messangebox.Name = "messangebox";
            this.messangebox.Size = new System.Drawing.Size(0, 13);
            this.messangebox.TabIndex = 5;
            // 
            // addnewvisitor_button
            // 
            this.addnewvisitor_button.Location = new System.Drawing.Point(541, 54);
            this.addnewvisitor_button.Name = "addnewvisitor_button";
            this.addnewvisitor_button.Size = new System.Drawing.Size(146, 29);
            this.addnewvisitor_button.TabIndex = 6;
            this.addnewvisitor_button.Text = "add new visitor";
            this.addnewvisitor_button.UseVisualStyleBackColor = true;
            this.addnewvisitor_button.Click += new System.EventHandler(this.addnewvisitor_button_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(825, 261);
            this.Controls.Add(this.addnewvisitor_button);
            this.Controls.Add(this.messangebox);
            this.Controls.Add(this.detele_visitors_button);
            this.Controls.Add(this.delete_doctors_button);
            this.Controls.Add(this.visitors_button);
            this.Controls.Add(this.doctors_button);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button doctors_button;
        private System.Windows.Forms.Button visitors_button;
        private System.Windows.Forms.Button delete_doctors_button;
        private System.Windows.Forms.Button detele_visitors_button;
        private System.Windows.Forms.Label messangebox;
        private System.Windows.Forms.Button addnewvisitor_button;
    }
}

